

-- Best practice MySQL as of 5.7.6
--
-- Hospial DB must exist before anything else is run.
-- Once run once, everything else will operate properly.
--
-- This script needs to run only once

DROP DATABASE IF EXISTS HOSPITALDB;
CREATE DATABASE HOSPITALDB;

USE HOSPITALDB;

DROP USER IF EXISTS Backup@localhost;
CREATE USER Backup@'localhost' IDENTIFIED WITH mysql_native_password BY 'Lolmasta12' REQUIRE NONE;
GRANT ALL ON HOSPITALDB.* TO Backup@'localhost';



--FLUSH PRIVILEGES;
