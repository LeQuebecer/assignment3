/**
 * Sample Skeleton for 'NewMasterTabPaneFXML.fxml' Controller Class
 */
package com.mycompany.assignment3.controller;

import com.mycompany.assignment3.CRUD.InpatientDAO;
import com.mycompany.assignment3.CRUD.MedicationDAO;
import com.mycompany.assignment3.CRUD.PatientDAO;
import com.mycompany.assignment3.CRUD.PatientDAONew;
import com.mycompany.assignment3.CRUD.SurgicalDAO;
import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.MedicationBean;
import com.mycompany.assignment3.beansFX.PatientBean;
import com.mycompany.assignment3.beansFX.SurgicalBean;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.BigDecimalStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MasterTabFXMLController implements Initializable {

    private final static Logger LOG = LoggerFactory.getLogger(MasterTabFXMLController.class);

    PatientBean patient;
    InpatientBean inpatient;
    MedicationBean medication;
    SurgicalBean surgical;
    PatientDAO patientFile;
    PatientDAONew patientNewFile;
    InpatientDAO inpatientFile;
    PatientBean superPatient;
    MedicationDAO medicalFile;
    SurgicalDAO surgicalFile;
    //InpatientBean initialize;
    //PatientBean medicalPatient, surgicalPatient, inpatientPatient;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="PatientTab"
    private Tab PatientTab; // Value injected by FXMLLoader

    @FXML // fx:id="PatientAnchor"
    private AnchorPane PatientAnchor; // Value injected by FXMLLoader

    @FXML // fx:id="patientIDTextField"
    private TextField patientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="lastNameTextField"
    private TextField lastNameTextField; // Value injected by FXMLLoader

    @FXML // fx:id="firstNameTextField"
    private TextField firstNameTextField; // Value injected by FXMLLoader

    @FXML // fx:id="diagnosisTextField"
    private TextField diagnosisTextField; // Value injected by FXMLLoader

    @FXML // fx:id="admissionDatePicker"
    private DatePicker admissionDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="releaseDatePicker"
    private DatePicker releaseDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="InpatientTab"
    private Tab InpatientTab; // Value injected by FXMLLoader

    @FXML // fx:id="InpatientAnchor"
    private AnchorPane InpatientAnchor; // Value injected by FXMLLoader

    @FXML // fx:id="inpatientIDTextField"
    private TextField inpatientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="roomNumberTextField"
    private TextField roomNumberTextField; // Value injected by FXMLLoader

    @FXML // fx:id="dailyRateTextField"
    private TextField dailyRateTextField; // Value injected by FXMLLoader

    @FXML // fx:id="servicesTextField"
    private TextField servicesTextField; // Value injected by FXMLLoader

    @FXML // fx:id="timeOfStayDatePicker"
    private DatePicker timeOfStayDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="inpatientSuppliesTextField"
    private TextField inpatientSuppliesTextField; // Value injected by FXMLLoader

    @FXML // fx:id="inpatientPatientIDTextField"
    private TextField inpatientPatientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="MedicalTab"
    private Tab MedicalTab; // Value injected by FXMLLoader

    @FXML // fx:id="MedicalAnchor"
    private AnchorPane MedicalAnchor; // Value injected by FXMLLoader

    @FXML // fx:id="medIDTextFIeld"
    private TextField medIDTextFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="medPatientIDTextField"
    private TextField medPatientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="medUnitsTextField"
    private TextField medUnitsTextField; // Value injected by FXMLLoader

    @FXML // fx:id="medUnitCostTextField"
    private TextField medUnitCostTextField; // Value injected by FXMLLoader

    @FXML // fx:id="medDescTextField"
    private TextField medDescTextField; // Value injected by FXMLLoader

    @FXML // fx:id="medDatePicker"
    private DatePicker medDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="SurgeryTab"
    private Tab SurgeryTab; // Value injected by FXMLLoader

    @FXML // fx:id="SurgicalAnchor"
    private AnchorPane SurgicalAnchor; // Value injected by FXMLLoader

    @FXML // fx:id="surgeryPatientIDTextField"
    private TextField surgeryPatientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgeryIDTextField"
    private TextField surgeryIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgeryDatePicker"
    private DatePicker surgeryDatePicker; // Value injected by FXMLLoader

    @FXML // fx:id="surgeryDescTextField"
    private TextField surgeryDescTextField; // Value injected by FXMLLoader

    @FXML // fx:id="roomFeeTextField"
    private TextField roomFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgeonFeeTextField"
    private TextField surgeonFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="suppliesTextField"
    private TextField suppliesTextField; // Value injected by FXMLLoader

    public MasterTabFXMLController() {
        patient = new PatientBean();
        inpatient = new InpatientBean();
        patientFile = new PatientDAO();
        patientNewFile = new PatientDAONew();
        inpatientFile = new InpatientDAO();
        superPatient = new PatientBean();
        medicalFile = new MedicationDAO();
        medication = new MedicationBean();
        surgicalFile = new SurgicalDAO();
        surgical = new SurgicalBean();
        //medicalPatient = new PatientBean();
        //surgicalPatient = new PatientBean();
        //inpatientPatient = new PatientBean();
    }

//    @FXML
//    void initializePatientIDInpatient(ActionEvent event) {
//       LOG.debug("I have arrived");
//        System.out.println("I have arrived");
//    }
    
    
    @FXML
    void clearFieldsInpatient(ActionEvent event) {
        inpatient.setPatientID(-1);
        inpatient.setInpatientID(-1);
        inpatient.setDailyRate(BigDecimal.valueOf(0.00));
        inpatient.setRoomNumber(" ");
        inpatient.setServices(BigDecimal.valueOf(0.00));
        inpatient.setSupplies(BigDecimal.valueOf(0.00));

    }

    @FXML
    void clearFieldsMedication(ActionEvent event) {
        medication.setPatientID(-1);
        medication.setMedicalID(-1);
        medication.setMedDescription(" ");
        medication.setMedUnitCost(BigDecimal.valueOf(0.00));
        medication.setMedUnits(BigDecimal.valueOf(0.00));
    }

    @FXML
    void clearPatient(ActionEvent event) {
        patient.setPatientID(-1);
        patient.setFirstName(" ");
        patient.setLastName(" ");
        patient.setDiagnosis(" ");
        patient.setAdmissionsTimeStampValue(Timestamp.valueOf(LocalDateTime.now()));
        patient.setReleaseTimeStampValue(Timestamp.valueOf(LocalDateTime.now()));
    }

    @FXML
    void clearSurgery(ActionEvent event) {
        surgical.setPatientID(-1);
        surgical.setSurgicalID(-1);
        surgical.setSurgeryDescription(" ");
        surgical.setSurgeonFee(BigDecimal.valueOf(0.00));
        surgical.setRoomFee(BigDecimal.valueOf(0.00));
        surgical.setSupplies(BigDecimal.valueOf(0.00));
    }

    @FXML
    void deleteInpatient(ActionEvent event) {
        try {

            inpatientFile.deleteInpatient(inpatient);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deleteMedical(ActionEvent event) {
        try {

            medicalFile.removeMedicationFile(medication);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deletePatient(ActionEvent event) {
        try {

            patientNewFile.removePatientFile(patient);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void deleteSurgical(ActionEvent event) {
        try {

            surgicalFile.removeSurgical(surgical);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void exit(ActionEvent event) {
        System.out.println("I Clicked Exit");
    }

    @FXML
    void generateReport(ActionEvent event) {

    }

    @FXML
    void nextInpatient(ActionEvent event) {
        try {
            //System.out.println("inpatientPatient in next patient" + inpatientPatient.getPatientID());
            inpatientFile.findNextByID( patient.getPatientID(),inpatient);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void nextMedical(ActionEvent event) {
        try {
            medicalFile.findNextByID(patient.getPatientID(), medication);

        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void nextPatient(ActionEvent event) {
        try {
            patient = patientNewFile.findNextByID(patient);
//            medicalPatient = superPatient;
//            System.out.println("medicalPatient " + medicalPatient.getPatientID());
//            inpatientPatient = superPatient;
//            System.out.println("inpatientPatient " + inpatientPatient.getPatientID());
//            surgicalPatient = superPatient;
//            System.out.println("surgicalPatient " + surgicalPatient.getPatientID());
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
        System.out.println("inpatientPatient object is " + superPatient.getFirstName());

    }

    @FXML
    void nextSurgical(ActionEvent event) {
        try {
            surgicalFile.findNextSurgicalID(surgical);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void prevInpatient(ActionEvent event) {
        try {
            inpatientFile.findPrevByID(superPatient, inpatient);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void prevMedical(ActionEvent event) {
        try {
            medicalFile.findPrevById(superPatient, medication);

        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void prevPatient(ActionEvent event) {
        try {
            superPatient = patientNewFile.findPrevByID(patient);

        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void prevSurgical(ActionEvent event) {
        try {
            surgicalFile.findPrevSurgicalID(surgical);

        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void saveInpatient(ActionEvent event) {
        try {
            int records = inpatientFile.saveInPatient(inpatient);
            LOG.info("Records updated or saved = " + records);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void saveMedical(ActionEvent event) {
        try {
            int records = medicalFile.saveMedication(medication);
            LOG.info("Records updated or saved = " + records);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void savePatient(ActionEvent event) {
        try {
            int records = patientNewFile.savePatient(patient);
            LOG.info("Records updated or saved = " + records);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }

    }

    @FXML
    void saveSurgical(ActionEvent event) {
        try {
            int records = surgicalFile.saveSurgical(surgical);
            LOG.info("Records updated or saved = " + records);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        denyIDEdits();
        lastNameTextField.focusedProperty().addListener(this::listenForLastNameFocus);
        lastNameTextField.textProperty().addListener(this::listenForLastNameChange);
        firstNameTextField.focusedProperty().addListener(this::listenForLastNameFocus);
        dailyRateTextField.textProperty().addListener(this::changed);

        Bindings.bindBidirectional(patientIDTextField.textProperty(), patient.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(lastNameTextField.textProperty(), patient.lastNameProperty());
        Bindings.bindBidirectional(firstNameTextField.textProperty(), patient.firstNameProperty());
        Bindings.bindBidirectional(diagnosisTextField.textProperty(), patient.diagnosisProperty());
        Bindings.bindBidirectional(admissionDatePicker.valueProperty(), patient.admissionsDateProperty());
        Bindings.bindBidirectional(releaseDatePicker.valueProperty(), patient.releaseDateProperty());

        Bindings.bindBidirectional(inpatientIDTextField.textProperty(), inpatient.inpatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(inpatientPatientIDTextField.textProperty(), inpatient.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(roomNumberTextField.textProperty(), inpatient.roomNumberProperty());
        Bindings.bindBidirectional(dailyRateTextField.textProperty(), inpatient.dailyRateProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(inpatientSuppliesTextField.textProperty(), inpatient.suppliesProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(servicesTextField.textProperty(), inpatient.servicesProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(timeOfStayDatePicker.valueProperty(), inpatient.dateTimeOfStayProperty());

        Bindings.bindBidirectional(medPatientIDTextField.textProperty(), medication.medPatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(medIDTextFIeld.textProperty(), medication.medicalIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(medDescTextField.textProperty(), medication.medDescriptionProperty());
        Bindings.bindBidirectional(medUnitCostTextField.textProperty(), medication.medUnitCostProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(medUnitsTextField.textProperty(), medication.medUnitsProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(medDatePicker.valueProperty(), medication.dateOfMedProperty());

        Bindings.bindBidirectional(surgeryPatientIDTextField.textProperty(), surgical.patientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(surgeryIDTextField.textProperty(), surgical.surgicalIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(surgeryDescTextField.textProperty(), surgical.surgeryDescriptionProperty());
        Bindings.bindBidirectional(roomFeeTextField.textProperty(), surgical.roomFeeProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(surgeonFeeTextField.textProperty(), surgical.surgeonFeeProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(suppliesTextField.textProperty(), surgical.suppliesProperty(), new BigDecimalStringConverter());
        Bindings.bindBidirectional(surgeryDatePicker.valueProperty(), surgical.surgeryDateProperty());

        //Initializes the First Record Off the Bat when Program Opens Up
        try {
            patient = patientNewFile.findNextByID(patient);
            inpatientFile.findNextByID(patient.getPatientID(), inpatient);
        }catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
        
    }

    private void denyIDEdits() {
        patientIDTextField.setEditable(false);
        inpatientPatientIDTextField.setEditable(false);
        inpatientIDTextField.setEditable(false);
        medPatientIDTextField.setEditable(false);
        medIDTextFIeld.setEditable(false);
        surgeryPatientIDTextField.setEditable(false);
        surgeryIDTextField.setEditable(false);

    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ErrorDAOTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ErrorDAOText"));
        dialog.setContentText(msg);
        dialog.show();
    }

    private void listenForLastNameFocus(Observable observable) {
        if (!lastNameTextField.isFocused()) {
            if (lastNameTextField.getText().length() > 30) {
                lastNameTextField.setText("TOO LONG");
            }
            LOG.info("Lost Focus");
        } else {
            LOG.info("Gain Focus");

        }
    }

    private void listenForLastNameChange(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue.length() > 30) {
            lastNameTextField.setText("Unacceptable Length");

        }
    }

    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (!newValue.matches("\\d{0,10}([\\.]\\d{0,2})?")) {
            dailyRateTextField.setText(oldValue);
        }
    }

//    public void checked(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//        if (newValue.matches("\\d{0,10})?")) {
//            firstNameTextField.setText(oldValue);
//        }
}
