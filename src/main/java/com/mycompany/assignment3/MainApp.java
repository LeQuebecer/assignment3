/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3;

import com.mycompany.assignment3.CRUD.PatientDAO;
import com.mycompany.assignment3.beansFX.PatientBean;
import com.mycompany.assignment3.controller.MasterTabFXMLController;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class MainApp extends Application {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Stage primaryStage;
    private PatientBean patient;
    private PatientDAO instance;

    private AnchorPane patientPane;
    private AnchorPane inpatientPane;
    private AnchorPane medicalPane;
    private AnchorPane surgicalPane;
    private TabPane masterPane;

    private MasterTabFXMLController masterController;

    /**
     * Where it all begins
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        initRootLayout();

        Scene scene = new Scene(masterPane);
        primaryStage.setScene(scene);
        primaryStage.show();
        LOG.info("Program started");
    }

    /**
     * Set up the master layout
     */
    public void initRootLayout() {

        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            //loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MainApp.class
                    .getResource("/fxml/MasterTabFXML.fxml"));
            masterPane = (TabPane) loader.load();
            masterController = loader.getController();
            // Send a reference to the masterController so it can change the 
            // center of the borderpane
            //masterController.setMasterPane(patientPane);


        } catch (IOException ex) {
            LOG.error(null, ex);
            LOG.debug("initRootLayout");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
//    private void errorAlert(String msg) {
//        Alert dialog = new Alert(Alert.AlertType.ERROR);
//        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("error"));
//        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("problem1"));
//        dialog.setContentText(ResourceBundle.getBundle("MessagesBundle").getString(msg));
//        dialog.show();
//    }
}
