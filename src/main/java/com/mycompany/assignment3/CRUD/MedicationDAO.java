/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.CRUD;

import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.MedicationBean;
import com.mycompany.assignment3.beansFX.PatientBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class MedicationDAO {

    private final static Logger LOG = LoggerFactory.getLogger(PatientDAO.class);
    String url = "jdbc:mysql://localhost:3306/hospitaldb";
    String user = "Backup";
    String password = "Lolmasta12";

    public MedicationBean findNextByID(int patientID, MedicationBean medical) throws SQLException {
        //Big Question - How do I cycle through
        String selectQuery = "SELECT PATIENTID, ID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE ID = (SELECT MIN(ID) from MEDICATION WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, medical.getMedicalID());
            //pStatement.setInt(2, patient.getPatientID());

            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    makeMedicalRec(resultSet, medical);
                }
            }
        }

        return medical;
    }

    public MedicationBean findPrevById(PatientBean patientID, MedicationBean medical) throws SQLException {
        //Big Question - How do I cycle through
        String selectQuery = "SELECT PATIENTID, ID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE ID = (SELECT MAX(ID) from MEDICATION WHERE ID < ?) ORDER BY PATIENTID";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, medical.getMedicalID());
            //pStatement.setInt(2, patientID);

            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    makeMedicalRec(resultSet, medical);
                }
            }
        }

        return medical;
    }

    /**
     * Creates an MedicalBean
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int createMedical(MedicationBean medical) throws SQLException {
        int records;
        String sql = "INSERT INTO MEDICATION(PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, medical.getPatientID());
            pStatement.setTimestamp(2, medical.getMedTimeStampValue());
            pStatement.setString(3, medical.getMedDescription());
            pStatement.setBigDecimal(4, medical.getMedUnitCost());
            pStatement.setBigDecimal(5, medical.getMedUnits());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                medical.setMedicalID(recordNum);
                //LOG.debug("New medical record ID is " + recordNum);

            } catch (SQLException ex) {
                //LOG.debug("Create error " + ex.getMessage());
                throw ex;
            }
        }
        return records;
    }

    /**
     * Updates the Medical Table in SQL
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int updateMedical(MedicationBean medical) throws SQLException {
        int records;
        String query = "UPDATE MEDICATION SET DATEOFMED = ?, MED = ?, UNITCOST = ?, UNITS = ? WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, medical.getMedTimeStampValue());
            pStatement.setString(2, medical.getMedDescription());
            pStatement.setBigDecimal(3, medical.getMedUnitCost());
            pStatement.setBigDecimal(4, medical.getMedUnits());
            pStatement.setInt(5, medical.getMedicalID());
            pStatement.setInt(6, medical.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Deletes Medical Record
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int deleteMedical(MedicationBean medical) throws SQLException {
        int records;
        String query = "DELETE FROM MEDICATION WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, medical.getMedicalID());
            LOG.debug("Medication to be deleted");
            records = pStatement.executeUpdate();
            LOG.debug("Medication should be " + records);
        }
        return records;
    }

    public int saveMedication(MedicationBean medication) throws SQLException {
        int records;
        if (medication.getPatientID() == -1) {
            System.out.println("medical " + medication.getPatientID());
            records = createMedical(medication);
        } else {
            records = updateMedical(medication);
        }
        return records;
    }

    public int removeMedicationFile(MedicationBean medication) throws SQLException {
        int records;
        if (medication.getPatientID() == -1) {
            records = 0;
            System.out.println("No record to delete");
        } else {
            records = deleteMedical(medication);
        }
        return records;
    }

    /**
     * Creates MedicationBean Object
     */
    private MedicationBean makeMedicalRec(ResultSet rs, MedicationBean medical) throws SQLException {

        medical.setMedicalID(rs.getInt("ID"));
        medical.setPatientID(rs.getInt("PATIENTID"));
        medical.setMedTimeStampValue(rs.getTimestamp("DATEOFMED"));
        medical.setMedDescription(rs.getString("MED"));
        medical.setMedUnitCost(rs.getBigDecimal("UNITCOST"));
        medical.setMedUnits(rs.getBigDecimal("UNITS"));

        return medical;
    }

}
