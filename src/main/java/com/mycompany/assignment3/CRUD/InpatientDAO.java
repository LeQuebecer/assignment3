/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.CRUD;

import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.PatientBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class InpatientDAO {

    private final static Logger LOG = LoggerFactory.getLogger(PatientDAO.class);
    String url = "jdbc:mysql://localhost:3306/hospitaldb";
    String user = "Backup";
    String password = "Lolmasta12";

    public InpatientBean findNextByID(int patientID, InpatientBean inpatient) throws SQLException {
       System.out.println("InPatient " + inpatient.getInpatientID());
        System.out.println("PatientID " + patientID);
        
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE ID = (SELECT MIN(ID) from INPATIENT WHERE ID > ?) AND PATIENTID = ?";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, inpatient.getInpatientID());
            //System.out.println("Inpatient ID from inpatientPatient in SQL Statement " + patient.getPatientID());
            pStatement.setInt(2, patientID);
            System.out.println("Patient ID from inpatientPatient in SQL Statement " + inpatient.getInpatientID());
            System.out.println("statement is " + pStatement.toString());
            
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundInpatientData(resultSet, inpatient);
                }
            }
        }
        System.out.println("I HAVE FOUND " + inpatient.getInpatientID() + " " + inpatient.getRoomNumber());
        return inpatient;
    }

    public InpatientBean findPrevByID(PatientBean patient, InpatientBean inpatient) throws SQLException {
         //Big Question - How do I cycle through
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE PATIENTID = ? AND ID = (SELECT MAX(ID) from INPATIENT WHERE ID < ?) ";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, patient.getPatientID());
            pStatement.setInt(2, inpatient.getInpatientID());
            
            
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    createBoundInpatientData(resultSet, inpatient);
                }
            }
        }
        System.out.println("I HAVE FOUND " + inpatient.getInpatientID() + " " + inpatient.getRoomNumber());
        return inpatient;
    }
    
     /**
     * Creates an InpatientBean
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int createInpatient(InpatientBean inpatient) throws SQLException {
        int records;
        LOG.debug("CREATE INPATIENT START");
        String sql = "INSERT INTO INPATIENT (PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) VALUES (?, ?, ?, ?, ?, ?)";
        LOG.debug("CREATE INPATIENT AFTER SQL STATEMENT");
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, inpatient.getPatientID());
            pStatement.setTimestamp(2, inpatient.getTimeOfStayTimeStampValue());
            pStatement.setString(3, inpatient.getRoomNumber());
            pStatement.setBigDecimal(4, inpatient.getDailyRate());
            pStatement.setBigDecimal(5, inpatient.getSupplies());
            pStatement.setBigDecimal(6, inpatient.getServices());
            LOG.debug("CREATE INPATIENT BEFORE EXECUTE UPDATE");
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                LOG.debug("CREATE INPATIENT IN THE TRY STATEMENT");
                int recordNum = -1;
                while (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                inpatient.setInpatientID(recordNum);
                //LOG.debug("New record ID is " + recordNum);
            } catch (SQLException ex) {
                //LOG.debug("Create error", ex.getMessage());
                throw ex;
            }
        }
        LOG.debug("CREATE INPATIENT BEFORE RETURN RECORDS");
        return records;
    } // FIXED
    
    
    /**
     * Updates the Inpatient Table in SQL
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int updateInpatient(InpatientBean inpatient) throws SQLException {
        int records;
        String query = "UPDATE INPATIENT SET DATEOFSTAY = ?, ROOMNUMBER = ?, DAILYRATE = ?, SERVICES = ?, SUPPLIES = ? WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, inpatient.getTimeOfStayTimeStampValue());
            pStatement.setString(2, inpatient.getRoomNumber());
            LOG.debug("Room Number during Update stage is " + inpatient.getRoomNumber());
            pStatement.setBigDecimal(3, inpatient.getDailyRate());
            pStatement.setBigDecimal(4, inpatient.getServices());
            pStatement.setBigDecimal(5, inpatient.getSupplies());
            pStatement.setInt(6, inpatient.getInpatientID());
            pStatement.setInt(7, inpatient.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;

    } // FIXED
    
    
    /**
     * Deletes Inpatient Record
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int deleteInpatient(InpatientBean inpatient) throws SQLException {
        //Can I just do Delete Inpatient where Patient ID = ? and Inpatient ID = ? That would super, SUPER simple. Almost TOO simple
        int records;
        String query = "DELETE FROM INPATIENT WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, inpatient.getInpatientID());
            LOG.debug("Inpatient to be deleted" + inpatient.getInpatientID());
            records = pStatement.executeUpdate();
            LOG.debug("Inpatient should be " + inpatient.getInpatientID());
        }
        return records;
    }
    
    public int saveInPatient(InpatientBean inpatient) throws SQLException {
        int records;
        if (inpatient.getPatientID() == -1) {
            records = createInpatient(inpatient);
        } else {
            records = updateInpatient(inpatient);
        }
        return records;
    }

    public int removePatientFile(InpatientBean inpatient) throws SQLException {
        int records;
        if (inpatient.getPatientID() == -1) {
            records = 0;
            System.out.println("No record to delete");
        } else {
            records = deleteInpatient(inpatient);
        }
        return records;
    }
    
    
    

    /**
     * Creates InpatientBean Object
     */
    private InpatientBean createBoundInpatientData(ResultSet rs, InpatientBean inpatient) throws SQLException {
        //Will I need a PatientID here? YES
        inpatient.setInpatientID(rs.getInt("ID"));
        inpatient.setPatientID(rs.getInt("PATIENTID"));
        inpatient.setTimeofStay(rs.getTimestamp("DATEOFSTAY"));
        inpatient.setRoomNumber(rs.getString("ROOMNUMBER"));
        inpatient.setServices(rs.getBigDecimal("SERVICES"));
        inpatient.setDailyRate(rs.getBigDecimal("DAILYRATE"));
        inpatient.setSupplies(rs.getBigDecimal("SUPPLIES"));
        return inpatient;
    }

}
