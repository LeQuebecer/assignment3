package com.mycompany.assignment3.CRUD;

import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.MedicationBean;
import com.mycompany.assignment3.beansFX.PatientBean;
import com.mycompany.assignment3.beansFX.SurgicalBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class PatientDAONew {

    private final static Logger LOG = LoggerFactory.getLogger(PatientDAO.class);

    //Patient File is the Master File.
    //The creation aspect is the same as almost any of the other ones.
    //However it's DELETION and UPDATE 
    String url = "jdbc:mysql://localhost:3306/hospitaldb";
    String user = "Backup";
    String password = "Lolmasta12";

    public PatientBean findNextByID(PatientBean patient) throws SQLException {

        String selectQuery = "SELECT PATIENTID, LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) from PATIENT WHERE PATIENTID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, patient.getPatientID());
            System.out.println("patient " + patient.getPatientID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundPatientData(resultSet, patient);
                }
            }
        }
        LOG.info("Found " + patient.getPatientID());
        return patient;
    }

    public PatientBean findPrevByID(PatientBean patient) throws SQLException {

        String selectQuery = "SELECT PATIENTID, LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MAX(PATIENTID) from PATIENT WHERE PATIENTID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, patient.getPatientID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundPatientData(resultSet, patient);
                }
            }
        }
        LOG.info("Found " + patient.getPatientID());
        return patient;
    }

    public int createPatient(PatientBean patient) throws SQLException {
        int records;
        String sql = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionsTimeStampValue());
            pStatement.setTimestamp(5, patient.getReleaseTimeStampValue());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                patient.setPatientID(recordNum);
                LOG.debug("PatientID is " + patient.getPatientID());
            } catch (SQLException ex) {
                LOG.debug("END OF CREATE PATIENT METHOD");
                throw ex;
            }
        }

        return records;
    }

    public int updatePatient(PatientBean patient) throws SQLException {
        int records;
        String query = "UPDATE PATIENT SET LASTNAME = ?, FIRSTNAME = ?, DIAGNOSIS = ?, ADMISSIONDATE = ?, RELEASEDATE =? WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionsTimeStampValue());
            pStatement.setTimestamp(5, patient.getReleaseTimeStampValue());
            pStatement.setInt(6, patient.getPatientID());
            records = pStatement.executeUpdate();
        }

        return records;
    }

    public int deletePatient(PatientBean patient) throws SQLException {
        int records;
        //In deletion, I simply accept the Bean, wipe out the subrecords before I do anything else.
        //I have to delete subrecords first, because otherwise I'll wind up having no PatientID 
        //In these Sub Methods, do I need an try while rs.next() - That might a lot of sense
        //deleteInpatientbyPatientID(patient);
        //deleteMedicalbyPatientID(patient);
        //deleteSurgicalbyPatientID(patient);
        String query = "DELETE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        LOG.debug("Records count is " + records);
        return records;

    }

    public int savePatient(PatientBean patient) throws SQLException {
        int records;
        if (patient.getPatientID() == -1) {
            records = createPatient(patient);
        } else {
            records = updatePatient(patient);
        }
        return records;
    }

    public int removePatientFile(PatientBean patient) throws SQLException {
        int records;
        if (patient.getPatientID() == -1) {
            records = 0;
            System.out.println("No record to delete");
        } else {
            records = deletePatient(patient);
        }
        return records;
    }

    private PatientBean createBoundPatientData(ResultSet rs, PatientBean patient) throws SQLException {
        patient.setPatientID(rs.getInt("PATIENTID"));
        patient.setLastName(rs.getString("LASTNAME"));
        patient.setFirstName(rs.getString("FIRSTNAME"));
        patient.setDiagnosis(rs.getString("DIAGNOSIS"));
        patient.setAdmissionsTimeStampValue(rs.getTimestamp("ADMISSIONDATE"));
        patient.setReleaseTimeStampValue(rs.getTimestamp("RELEASEDATE"));
        return patient;
    }

}
