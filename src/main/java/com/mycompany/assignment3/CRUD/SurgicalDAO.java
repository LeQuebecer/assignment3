/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.CRUD;

import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.SurgicalBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class SurgicalDAO {

    private final static Logger LOG = LoggerFactory.getLogger(PatientDAO.class);
    String url = "jdbc:mysql://localhost:3306/hospitaldb";
    String user = "Backup";
    String password = "Lolmasta12";

    /**
     * Find Surgical Record via Surgical ID
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public SurgicalBean findNextSurgicalID(SurgicalBean surgical) throws SQLException {
        //Big Question - How do I cycle through
        String selectQuery = "SELECT ID,PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = (SELECT MIN(ID) from SURGICAL WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, surgical.getSurgicalID());
            //pStatement.setInt(2, patient.getPatientID());
            
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    makeSurgicalRec(resultSet, surgical);
                }
            }
        }
        //System.out.println("I HAVE FOUND " + inpatient.getInpatientID() + " " + inpatient.getRoomNumber());
        return surgical;
    }

    public SurgicalBean findPrevSurgicalID(SurgicalBean surgical) throws SQLException {

        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID,PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = (SELECT MAX(ID) from SURGICAL WHERE ID < ?)";
        //"SELECT ID,PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = (SELECT MAX(ID) from SURGICAL WHERE ID < ?)""

        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, surgical.getSurgicalID());
            LOG.debug("PatientID Find Surgical is " + surgical.getSurgicalID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    surgical = makeSurgicalRec(resultSet, surgical);
                    //Either here, or within each method, the results need to be added to the Lists.
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //Is this what I should be returning?
        return surgical;
    }

    /**
     * Creates an SurgicalBean
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int createSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String sql = "INSERT INTO SURGICAL(PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, surgical.getPatientID());
            pStatement.setTimestamp(2, surgical.getSurgeryDateTimeStampValue());
            pStatement.setString(3, surgical.getSurgeryDescription());
            pStatement.setBigDecimal(4, surgical.getRoomFee());
            pStatement.setBigDecimal(5, surgical.getSurgeonFee());
            pStatement.setBigDecimal(6, surgical.getSupplies());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                LOG.debug("SurgicalID primary unit is " + surgical.getPatientID());
                surgical.setSurgicalID(recordNum);
                LOG.debug("SurgicalID primary unit is " + surgical.getPatientID());
                //LOG.debug("New Record ID is " + recordNum);
            } catch (SQLException ex) {
                //LOG.debug("Create eror " + ex.getMessage());
                throw ex;
            }
        }
        return records;
    }

    /**
     * Updates the Surgical Table in SQL
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int updateSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String query = "UPDATE SURGICAL SET DATEOFSURGERY = ?, SURGERY = ?, ROOMFEE = ?, SURGERONFEE = ?, SUPPLIES = ?, WHERE SURGERYID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, surgical.getSurgeryDateTimeStampValue());
            pStatement.setString(2, surgical.getSurgeryDescription());
            pStatement.setBigDecimal(3, surgical.getRoomFee());
            pStatement.setBigDecimal(4, surgical.getSurgeonFee());
            pStatement.setBigDecimal(5, surgical.getSupplies());
            pStatement.setInt(6, surgical.getSurgicalID());
            pStatement.setInt(7, surgical.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Deletes Surgical Record
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int deleteSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String query = "DELETE FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, surgical.getSurgicalID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    public int saveSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        if (surgical.getPatientID() == -1) {
            records = createSurgical(surgical);
        } else {
            records = updateSurgical(surgical);
        }
        return records;
    }

    public int removeSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        if (surgical.getPatientID() == -1) {
            records = 0;
            System.out.println("No record to delete");
        } else {
            records = deleteSurgical(surgical);
        }
        return records;
    }

    /**
     * Creates SurgicalBean Object
     */
    private SurgicalBean makeSurgicalRec(ResultSet rs, SurgicalBean surgical) throws SQLException {
        surgical.setSurgicalID(rs.getInt("ID"));
        surgical.setPatientID(rs.getInt("PATIENTID"));
        surgical.setSurgeryDateTimeStampValue(rs.getTimestamp("DATEOFSURGERY"));
        surgical.setSurgeryDescription(rs.getString("SURGERY"));
        surgical.setRoomFee(rs.getBigDecimal("ROOMFEE"));
        surgical.setSurgeonFee(rs.getBigDecimal("SURGEONFEE"));
        surgical.setSupplies(rs.getBigDecimal("SUPPLIES"));
        return surgical;
    }

}
