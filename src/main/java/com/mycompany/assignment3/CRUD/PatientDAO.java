/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.CRUD;


import com.mycompany.assignment3.beansFX.InpatientBean;
import com.mycompany.assignment3.beansFX.MedicationBean;
import com.mycompany.assignment3.beansFX.PatientBean;
import com.mycompany.assignment3.beansFX.SurgicalBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slesperance
 */
public class PatientDAO {

    private final static Logger LOG = LoggerFactory.getLogger(PatientDAO.class);

    //Patient File is the Master File.
    //The creation aspect is the same as almost any of the other ones.
    //However it's DELETION and UPDATE 
    String url = "jdbc:mysql://localhost:3306/hospitaldb";
    String user = "Backup";
    String password = "Lolmasta12";
    PatientBean PatientBean;
    InpatientBean InpatientBean;
    MedicationBean MedicationBean;
    SurgicalBean SurgicalBean;


    //                  HERE BE THE FINDING STUFF SECTION                    HERE BE THE FINDING STUFF SECTION
    //Find all Patients - Assemble into a List.
    /**
     *
     * Finds all records and subrecords in HospitalDB
     *
     * @throws SQLException
     */
    public List<PatientBean> findAll() throws SQLException {
        List<PatientBean> rows = new ArrayList<>();

        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM Patient";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                PatientBean patient = makePatient(resultSet);
                //findAllGames(patient);
                //This here will need to be a set of three - one for Surgical, one for Inpatient, one for Medical 
                //findAllInPatientRecords(patient);
                //findAllSurgicalRecords(patient);
                //findAllMedicalRecords(patient);
                rows.add(patient);
            }
        } catch (SQLException ex) {
            //LOG.debug("findAll error", ex.getMessage());
            throw ex;
        }
        return rows;
    }

    /**
     *
     * @param patient
     * @return 
     * @Find Patient by Patient ID
     * @throws SQLException
     */
    public PatientBean findPatientID(PatientBean patient) throws SQLException {
        String selectQuery = "SELECT PATIENTID, LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) from PATIENT WHERE PATIENTID < ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient = makePatient(resultSet);
                    //Each method should cycle through it's results, put them into a list, return the list.
                    //Given the object is set to a reference point, and they are reading the same ref point, I'm good.
                    //findPatientInPatient(patient);
                    //findPatientMedical(patient);
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        return patient;
    }
//These might be void, provided they automatically update or add their contents to Patient's Lists.

    /**
     * Find Inpatient records by Patient ID
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientInPatient(PatientBean patient) throws SQLException {
        LOG.debug("I'm STARTINJG NOW");
        InpatientBean inpatient;
        List<InpatientBean> l = new ArrayList<InpatientBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            LOG.debug("ID" + patient.getPatientID());
            pStatement.setInt(1, patient.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.debug("I'm HERE");
                    inpatient = makeInpatientRec(resultSet);
                    l.add(inpatient);
                }
            }
        } catch (SQLException ex) {
            LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //patient.setInpatientList(l);
        return patient;
    }

    /**
     * Find Medical Record by Patient ID
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientMedical(PatientBean patient) throws SQLException {
        MedicationBean medical = new MedicationBean();
        List<MedicationBean> l = new ArrayList<MedicationBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                //I might be missing a for loop, but resultSet.next() Might solve this.
                while (resultSet.next()) {
                    medical = makeMedicalRec(resultSet);
                    l.add(medical);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //patient.setMedicalList(l);
        return patient;
    }

    /**
     * Find Surgical Record by Patient ID
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientSurgical(PatientBean patient) throws SQLException {
        //This might need to be overloaded, or provided with a secondary. Because right now I'm returning a patientBean. I want one to return Surgical
        SurgicalBean surgical = new SurgicalBean();
        List<SurgicalBean> l = new ArrayList<SurgicalBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    surgical = makeSurgicalRec(resultSet);
                    l.add(surgical);
                    //Either here, or within each method, the results need to be added to the Lists.
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //Is this what I should be returning?
        //patient.setSurgicalList(l);
        return patient;
    }

//FIND A SPECIFIC PATIERNT VIA LAST NAME
    /**
     * This method finds the Patient via last name.
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientLastName(PatientBean patient) throws SQLException {
        String selectQuery = "SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE LASTNAME = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, patient.getLastName());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    patient = makePatient(resultSet);
                    //Each method should cycle through it's results, put them into a list, return the list.
                    //Given the object is set to a reference point, and they are reading the same ref point, I'm good.
                    //findPatientInPatientLastName(patient);
                    //findPatientMedicalLastName(patient);
                    //findPatientSurgicalLastName(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        return patient;
    }
//These might be void, provided they automatically update or add their contents to Patient's Lists.

    /**
     * Find Inpatient by Patient via last name
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientInPatientLastName(PatientBean patient) throws SQLException {
        InpatientBean inpatient = new InpatientBean();
        List<InpatientBean> l = new ArrayList<InpatientBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID, PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE LASTNAME = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, patient.getLastName());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    inpatient = makeInpatientRec(resultSet);
                    l.add(inpatient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //patient.setInpatientList(l);
        return patient;
    } // FIXED

    /**
     * Find Medical Record by Patient via last name
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientMedicalLastName(PatientBean patient) throws SQLException {
        MedicationBean medical = new MedicationBean();
        List<MedicationBean> l = new ArrayList<MedicationBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                //I might be missing a for loop, but resultSet.next() Might solve this.
                while (resultSet.next()) {
                    medical = makeMedicalRec(resultSet);
                    l.add(medical);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        LOG.debug("L LIST SIZE IS " + l.size());
        //patient.setMedicalList(l);
        return patient;
    }

    /**
     * Find Surgical Record by via patient last name
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public PatientBean findPatientSurgicalLastName(PatientBean patient) throws SQLException {
        SurgicalBean surgical = new SurgicalBean();
        List<SurgicalBean> l = new ArrayList<SurgicalBean>();
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    surgical = makeSurgicalRec(resultSet);
                    l.add(surgical);
                    //Either here, or within each method, the results need to be added to the Lists.
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //Is this what I should be returning?
        //patient.setSurgicalList(l);
        return patient;
    }

    //OTHER FIND METHODS FOR INPATIENT, MEDICAL, SURGICAL
    /**
     * Find inpatient via inpatient ID
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public InpatientBean findInpatient(InpatientBean inpatient) throws SQLException {
        //LOG.debug("I'm STARTINJG NOW");
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, inpatient.getInpatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    //LOG.debug("I'm HERE");
                    inpatient = makeInpatientRec(resultSet);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        return inpatient;
    }

    /**
     * Find Surgical Record via Surgical ID
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public SurgicalBean findSurgical(SurgicalBean surgical) throws SQLException {

        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT PATIENTID, ID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, surgical.getPatientID());
            LOG.debug("PatientID Find Surgical is " + surgical.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    surgical = makeSurgicalRec(resultSet);
                    //Either here, or within each method, the results need to be added to the Lists.
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //Is this what I should be returning?
        return surgical;
    }

    /**
     * Find Medical Record via Medical ID
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public MedicationBean findMedical(MedicationBean medical) throws SQLException {
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT PATIENTID, ID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, medical.getPatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                //I might be missing a for loop, but resultSet.next() Might solve this.
                while (resultSet.next()) {
                    medical = makeMedicalRec(resultSet);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }

        return medical;
    }

    /**
     * Gathers Patient records, gets sub-records
     *
     * @param
     * @return Void
     * @throws SQLException
     */
    private void findAllRecords(PatientBean patient) throws SQLException {
        String selectQuery = "SELECT ID,SELECT PATIENTID,LASTNAME,FIRSTNAME,DIAGNOSIS,ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        //This line provides a connection
        try (Connection connection = DriverManager.getConnection(url, user, password);
                //Prepared Statement to protect against SQL Injection - Prevent someone from returning messed up code.
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            //This is the ACTUAL prepared statement. It sets a limit? Or does it auto-increment. More like first record?
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    //patient.getInpatientList().add(makeInpatientRec(resultSet));
                    //patient.getSurgicalList().add(makeSurgicalRec(resultSet));
                   // patient.getMedicalList().add(makeMedicalRec(resultSet));
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findAllGames error", ex.getMessage());
            throw ex;
        }
    }

    //                      THIS IS THE CREATION SECTION                      THIS IS THE CREATION SECTION
    /**
     * Creates a PatientBean
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int createPatient(PatientBean patient) throws SQLException {
        int records;
        //LOG.debug("CREATE PATIENT START");
        //I must be precise in the order of creation - Java must skew to SQL, or vice versa? Ask Omni.
        String sql = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionsTimeStampValue());
            pStatement.setTimestamp(5, patient.getReleaseTimeStampValue());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                patient.setPatientID(recordNum);
                LOG.debug("PatientID is " + patient.getPatientID());
            } catch (SQLException ex) {
                LOG.debug("END OF CREATE PATIENT METHOD");
                throw ex;
            }
        }

        return records;
    }

    /**
     * Creates an InpatientBean
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int createInpatient(InpatientBean inpatient) throws SQLException {
        int records;
        LOG.debug("CREATE INPATIENT START");
        String sql = "INSERT INTO INPATIENT (PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) VALUES (?, ?, ?, ?, ?, ?)";
        LOG.debug("CREATE INPATIENT AFTER SQL STATEMENT");
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, inpatient.getPatientID());
            pStatement.setTimestamp(2, inpatient.getTimeOfStayTimeStampValue());
            pStatement.setString(3, inpatient.getRoomNumber());
            pStatement.setBigDecimal(4, inpatient.getDailyRate());
            pStatement.setBigDecimal(5, inpatient.getSupplies());
            pStatement.setBigDecimal(6, inpatient.getServices());
            LOG.debug("CREATE INPATIENT BEFORE EXECUTE UPDATE");
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                LOG.debug("CREATE INPATIENT IN THE TRY STATEMENT");
                int recordNum = -1;
                while (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                inpatient.setInpatientID(recordNum);
                //LOG.debug("New record ID is " + recordNum);
            } catch (SQLException ex) {
                //LOG.debug("Create error", ex.getMessage());
                throw ex;
            }
        }
        LOG.debug("CREATE INPATIENT BEFORE RETURN RECORDS");
        return records;
    } // FIXED

    /**
     * Creates an SurgicalBean
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int createSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String sql = "INSERT INTO SURGICAL(PATIENTID,DATEOFSURGERY,SURGERY,ROOMFEE,SURGEONFEE,SUPPLIES) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, surgical.getPatientID());
            pStatement.setTimestamp(2, surgical.getSurgeryDateTimeStampValue());
            pStatement.setString(3, surgical.getSurgeryDescription());
            pStatement.setBigDecimal(4, surgical.getRoomFee());
            pStatement.setBigDecimal(5, surgical.getSurgeonFee());
            pStatement.setBigDecimal(6, surgical.getSupplies());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                LOG.debug("SurgicalID primary unit is " + surgical.getPatientID());
                surgical.setSurgicalID(recordNum);
                LOG.debug("SurgicalID primary unit is " + surgical.getPatientID());
                //LOG.debug("New Record ID is " + recordNum);
            } catch (SQLException ex) {
                //LOG.debug("Create eror " + ex.getMessage());
                throw ex;
            }
        }
        return records;
    }

    /**
     * Creates an MedicalBean
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int createMedical(MedicationBean medical) throws SQLException {
        int records;
        String sql = "INSERT INTO MEDICATION(PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setInt(1, medical.getPatientID());
            pStatement.setTimestamp(2, medical.getMedTimeStampValue());
            pStatement.setString(3, medical.getMedDescription());
            pStatement.setBigDecimal(4, medical.getMedUnitCost());
            pStatement.setBigDecimal(5, medical.getMedUnits());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                medical.setMedicalID(recordNum);
                //LOG.debug("New medical record ID is " + recordNum);

            } catch (SQLException ex) {
                //LOG.debug("Create error " + ex.getMessage());
                throw ex;
            }
        }
        return records;
    }

//          THIS IS THE UPDATE SECTION                                  THIS IS THE UPDATE SECTION
    /**
     * Updates the Patient Table in SQL
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int updatePatient(PatientBean patient) throws SQLException {
        int records;
        String query = "UPDATE PATIENT SET LASTNAME = ?, FIRSTNAME = ?, DIAGNOSIS = ?, ADMISSIONDATE = ?, RELEASEDATE =? WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionsTimeStampValue());
            pStatement.setTimestamp(5, patient.getReleaseTimeStampValue());
            pStatement.setInt(6, patient.getPatientID());
            records = pStatement.executeUpdate();
        }

        return records;
    }

    /**
     * Updates the Inpatient Table in SQL
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int updateInpatient(InpatientBean inpatient) throws SQLException {
        int records;
        String query = "UPDATE INPATIENT SET DATEOFSTAY = ?, ROOMNUMBER = ?, SUPPLIES = ?, SERVICES = ?, SUPPLIES = ? WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, inpatient.getTimeOfStayTimeStampValue());
            pStatement.setString(2, inpatient.getRoomNumber());
            LOG.debug("Room Number during Update stage is " + inpatient.getRoomNumber());
            pStatement.setBigDecimal(3, inpatient.getDailyRate());
            pStatement.setBigDecimal(4, inpatient.getServices());
            pStatement.setBigDecimal(5, inpatient.getSupplies());
            pStatement.setInt(6, inpatient.getInpatientID());
            pStatement.setInt(7, inpatient.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;

    } // FIXED

    /**
     * Updates the Surgical Table in SQL
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int updateSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String query = "UPDATE SURGICAL SET DATEOFSURGERY = ?, SURGERY = ?, ROOMFEE = ?, SURGERONFEE = ?, SUPPLIES = ?, WHERE SURGERYID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, surgical.getSurgeryDateTimeStampValue());
            pStatement.setString(2, surgical.getSurgeryDescription());
            pStatement.setBigDecimal(3, surgical.getRoomFee());
            pStatement.setBigDecimal(4, surgical.getSurgeonFee());
            pStatement.setBigDecimal(5, surgical.getSupplies());
            pStatement.setInt(6, surgical.getSurgicalID());
            pStatement.setInt(7, surgical.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Updates the Medical Table in SQL
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int updateMedical(MedicationBean medical) throws SQLException {
        int records;
        String query = "UPDATE MEDICATION SET DATEOFMED = ?, MED = ?, UNITCOST = ?, UNITS = ? WHERE ID = ? AND PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, medical.getMedTimeStampValue());
            pStatement.setString(2, medical.getMedDescription());
            pStatement.setBigDecimal(3, medical.getMedUnitCost());
            pStatement.setBigDecimal(4, medical.getMedUnits());
            pStatement.setInt(5, medical.getMedicalID());
            pStatement.setInt(6, medical.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

//          THIS IS THE DELETE SECTION                                      THIS IS THE DELETE SECTION
    //This method verifies if the entirety of it is gone.
    /**
     * Verifies the Patient and sub tables return no records
     *
     * @return
     * @throws SQLException
     */
    public int verifyPatientDeletion() throws SQLException {
        int count = -1;

        verifyInpatientDeletion();
        verifySurgicalDeletion();
        verifyMedicationDeletion();
        LOG.debug("VerifyPatientDeletion start");
        String selectQuery = "SELECT COUNT(*) FROM PATIENT";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            LOG.debug("VerifyPatientDeletion in the statement ");
            try (ResultSet resultSet = pStatement.executeQuery()) {
                resultSet.first();
                if (!resultSet.isBeforeFirst()) {
                    LOG.debug("VerifyPatientDeletion reached the IF statement");
                    count = 0;
                } else {
                    LOG.debug("VerifyPatientDeletion running loose in the else segment.");
                    count = 1;
                }
            }
        } catch (SQLException ex) {

            throw ex;
        }
        return count;
    }

    /**
     * Verifies the Subpatient table returns no records
     *
     * @return
     * @throws SQLException
     */
    public int verifyInpatientDeletion() throws SQLException {
        int count = -1;
        LOG.debug("VerifyInpatientDeletion start");
        String selectQuery = "SELECT COUNT(*) FROM INPATIENT";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            LOG.debug("VerifyInpatientDeletion in the statement ");
            try (ResultSet resultSet = pStatement.executeQuery()) {
                LOG.debug("Check" + resultSet.first());
                if (!resultSet.isBeforeFirst()) {
                    LOG.debug("VerifyInpatientDeletion reached the IF statement");
                    count = 0;
                    //count = resultSet.getRow();
                } else {
                    LOG.debug("VerifyInpatientDeletion running loose in the else segment.");
                    count = 1;
                }
            }
        } catch (SQLException ex) {

            throw ex;
        }
        return count;
    }

    /**
     * Verifies the Surgical table returns no records
     *
     * @return
     * @throws SQLException
     */
    public int verifySurgicalDeletion() throws SQLException {
        int count = -1;
        LOG.debug("VerifySurgicalDeletion start");
        String selectQuery = "SELECT COUNT(*) FROM SURGICAL";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            LOG.debug("VerifySurgicalDeletion in the statement ");
            try (ResultSet resultSet = pStatement.executeQuery()) {
                LOG.debug("Check" + resultSet.first());
                if (!resultSet.isBeforeFirst()) {
                    LOG.debug("VerifySurgicalDeletion reached the IF statement");
                    count = 0;
                } else {
                    LOG.debug("VerifySurgicalDeletion running loose in the else segment.");
                    count++;
                }
            }
        } catch (SQLException ex) {

            throw ex;
        }
        return count;
    }

    /**
     * Verifies the Medical table returns no records
     *
     * @return
     * @throws SQLException
     */
    public int verifyMedicationDeletion() throws SQLException {
        int count = -1;
        LOG.debug("VerifyMedicationDeletion start");
        String selectQuery = "SELECT COUNT(*) FROM MEDICATION";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            LOG.debug("VerifyMedicationDeletion in the statement ");
            try (ResultSet resultSet = pStatement.executeQuery()) {
                resultSet.first();
                if (!resultSet.isBeforeFirst()) {
                    LOG.debug("VerifyMedicationDeletion reached the IF statement");
                    count = 0;
                } else {
                    LOG.debug("VerifyMedicationDeletion running loose in the else segment.");
                    count = 1;
                }
            }
        } catch (SQLException ex) {

            throw ex;
        }
        return count;
    }

    /**
     * Deletes the Patient and subrecords connected to Patient
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int deletePatient(PatientBean patient) throws SQLException {
        int records;
        //In deletion, I simply accept the Bean, wipe out the subrecords before I do anything else.
        //I have to delete subrecords first, because otherwise I'll wind up having no PatientID 
        //In these Sub Methods, do I need an try while rs.next() - That might a lot of sense
        deleteInpatientbyPatientID(patient);
        deleteMedicalbyPatientID(patient);
        deleteSurgicalbyPatientID(patient);
        String query = "DELETE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        LOG.debug("Records count is " + records);
        return records;

    }

    /**
     * Deletes the Patient and subrecords connected to Patient
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int deleteInpatientbyPatientID(PatientBean patient) throws SQLException {
        //Can I just do Delete Inpatient where Patient ID = ? and Inpatient ID = ? That would super, SUPER simple. Almost TOO simple
        int records;
        String query = "DELETE FROM INPATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        //patient.getInpatientList().clear();
        return records;
    } // DOESN'T NEED TO BE FIXED

    /**
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int deleteMedicalbyPatientID(PatientBean patient) throws SQLException {
        int records;
        String query = "DELETE FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        //patient.getMedicalList().clear();
        return records;
    }

    /**
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    public int deleteSurgicalbyPatientID(PatientBean patient) throws SQLException {
        int records;
        String query = "DELETE FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        //patient.getSurgicalList().clear();
        return records;
    }

    /**
     * Deletes Inpatient Record
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int deleteInpatient(InpatientBean inpatient) throws SQLException {
        //Can I just do Delete Inpatient where Patient ID = ? and Inpatient ID = ? That would super, SUPER simple. Almost TOO simple
        int records;
        String query = "DELETE FROM INPATIENT WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, inpatient.getInpatientID());
            LOG.debug("Inpatient to be deleted" + inpatient.getInpatientID());
            records = pStatement.executeUpdate();
            LOG.debug("Inpatient should be " + inpatient.getInpatientID());
        }
        return records;
    }

    /**
     * Deletes Medical Record
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int deleteMedical(MedicationBean medical) throws SQLException {
        int records;
        String query = "DELETE FROM MEDICATION WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, medical.getMedicalID());
            LOG.debug("Medication to be deleted");
            records = pStatement.executeUpdate();
            LOG.debug("Medication should be " + records);
        }
        return records;
    }

    /**
     * Deletes Surgical Record
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int deleteSurgical(SurgicalBean surgical) throws SQLException {
        int records;
        String query = "DELETE FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, surgical.getSurgicalID());
            records = pStatement.executeUpdate();
        }
        return records;
    }

    /**
     * Creates PatientBean Object
     */
    private PatientBean makePatient(ResultSet rs) throws SQLException {
        PatientBean patient = new PatientBean();
        patient.setPatientID(rs.getInt("PATIENTID"));
        patient.setLastName(rs.getString("LASTNAME"));
        patient.setFirstName(rs.getString("FIRSTNAME"));
        patient.setDiagnosis(rs.getString("DIAGNOSIS"));
        patient.setAdmissionsTimeStampValue(rs.getTimestamp("ADMISSIONDATE"));
        patient.setReleaseTimeStampValue(rs.getTimestamp("RELEASEDATE"));
        //I'm probably missing stuff for the Lists. Do they need to be prepared here? I think they in fact, do. At the very least, they must be initialized.
        //They already are initialized, hence new PatientBean(). I do have one problem - how and where do I fill them up?
        return patient;
    }

    /**
     * Creates InpatientBean Object
     */
    private InpatientBean makeInpatientRec(ResultSet rs) throws SQLException {
        InpatientBean inpatient = new InpatientBean();
        //Will I need a PatientID here? YES
        inpatient.setInpatientID(rs.getInt("ID"));
        inpatient.setPatientID(rs.getInt("PATIENTID"));
        inpatient.setTimeofStay(rs.getTimestamp("DATEOFSTAY"));
        inpatient.setRoomNumber(rs.getString("ROOMNUMBER"));
        inpatient.setServices(rs.getBigDecimal("SERVICES"));
        inpatient.setDailyRate(rs.getBigDecimal("DAILYRATE"));
        inpatient.setSupplies(rs.getBigDecimal("SUPPLIES"));
        return inpatient;
    } 

    /**
     * Creates SurgicalBean Object
     */
    private SurgicalBean makeSurgicalRec(ResultSet rs) throws SQLException {
        SurgicalBean surgical = new SurgicalBean();
        surgical.setSurgicalID(rs.getInt("ID"));
        surgical.setPatientID(rs.getInt("PATIENTID"));
        surgical.setSurgeryDateTimeStampValue(rs.getTimestamp("DATEOFSURGERY"));
        surgical.setSurgeryDescription(rs.getString("SURGERY"));
        surgical.setRoomFee(rs.getBigDecimal("ROOMFEE"));
        surgical.setSurgeonFee(rs.getBigDecimal("SURGEONFEE"));
        surgical.setSupplies(rs.getBigDecimal("SUPPLIES"));
        return surgical;
    }

    /**
     * Creates MedicationBean Object
     */
    private MedicationBean makeMedicalRec(ResultSet rs) throws SQLException {
        MedicationBean medical = new MedicationBean();
        medical.setMedicalID(rs.getInt("ID"));
        medical.setPatientID(rs.getInt("PATIENTID"));
        medical.setMedTimeStampValue(rs.getTimestamp("DATEOFMED"));
        medical.setMedDescription(rs.getString("MED"));
        medical.setMedUnitCost(rs.getBigDecimal("UNITCOST"));
        medical.setMedUnits(rs.getBigDecimal("UNITS"));

        return medical;
    }

    /**
     * Checks for inpatient record after removal of record
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    public int findPatientInPatientAfterDelete(InpatientBean inpatient) throws SQLException {
        LOG.debug("I'm STARTINJG NOW");
        int returns = -1;
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT ID,PATIENTID,DATEOFSTAY,ROOMNUMBER,DAILYRATE,SUPPLIES, SERVICES FROM INPATIENT WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, inpatient.getInpatientID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (!resultSet.next()) {
                    LOG.debug("I'm HERE");
                    returns = resultSet.getRow();
                }
            }
        } catch (SQLException ex) {
            LOG.debug("findID error", ex.getMessage());
            throw ex;
        }

        return returns;
    }

    /**
     * Checks for Medical record after removal of record
     *
     * @param medical
     * @return
     * @throws SQLException
     */
    public int findPatientMedicalAfterDelete(MedicationBean medical) throws SQLException {
        int returns = -1;
        String selectQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, UNITS FROM MEDICATION WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, medical.getMedicalID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                //I might be missing a for loop, but resultSet.next() Might solve this.
                if (!resultSet.next()) {
                    returns = resultSet.getRow();
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }

        return returns;
    }

    /**
     * Checks for Surgical record after removal of record
     *
     * @param surgical
     * @return
     * @throws SQLException
     */
    public int findPatientSurgicalAfterDelete(SurgicalBean surgical) throws SQLException {
        int returns = -1;
        //This might need to be overloaded, or provided with a secondary. Because right now I'm returning a patientBean. I want one to return Surgical
        //YOU ARE SELECTING THE RECORD, NOT THE PATIENT, THE PATIENT IS ALREADY PATIENT - DO FOR ALL THREE TO RETRIEVE ALL RECORDS
        String selectQuery = "SELECT PATIENTID, ID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, surgical.getSurgicalID());

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (!resultSet.next()) {
                    returns = resultSet.getRow();
                    //Either here, or within each method, the results need to be added to the Lists.
                    //findPatientSurgical(patient);
                }
            }
        } catch (SQLException ex) {
            //LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        //Is this what I should be returning?

        return returns;
    }

    ///Extra Stuff Section
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

}
