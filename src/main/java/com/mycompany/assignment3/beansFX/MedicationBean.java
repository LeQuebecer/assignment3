/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.beansFX;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author slesperance
 */
public class MedicationBean {

    private IntegerProperty medicalID;
    private ObjectProperty<LocalDate> dateOfMed;
    private StringProperty medDescription;
    private ObjectProperty<BigDecimal> medUnitCost;
    private ObjectProperty<BigDecimal> medUnits;
    //PatientBean is MANDATORY - but only the Int
    private IntegerProperty patientID;

    //Design Decision - Both directions or not?
    //If your system requires, given an expense, you want to be able to find who it belongs to, then it needs
    //-a bidirectional.
    public MedicationBean(int patientID, int medicalID, Timestamp dateOfMed, String medDescription, BigDecimal medUnitCost, BigDecimal medUnits) {
        this.patientID = new SimpleIntegerProperty(patientID);
        this.medicalID = new SimpleIntegerProperty(medicalID);
        this.dateOfMed = new SimpleObjectProperty<>(LocalDate.now());
        this.medDescription = new SimpleStringProperty(medDescription);
        this.medUnitCost = new SimpleObjectProperty<>(medUnitCost);
        this.medUnits = new SimpleObjectProperty<>(medUnits);

    }

    // Require default constructor
    public MedicationBean() {
        this(0, 0, Timestamp.valueOf(LocalDateTime.now()), " ", BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00));
    }

    //Getters
    public final int getMedicalID() {
        return medicalID.get();
    }

    public final LocalDate getDateOfMed() {
        return dateOfMed.get();
    }

    public final String getMedDescription() {
        return medDescription.get();
    }

    public final BigDecimal getMedUnitCost() {
        return medUnitCost.get();
    }

    public final BigDecimal getMedUnits() {
        return medUnits.get();
    }

    public final int getPatientID() {
        return patientID.get();
    }

    //Setters
    public void setMedicalID(final int medicalID) {
        this.medicalID.set(medicalID);
    }

    public void setDateOfMed(final LocalDate dateOfMed) {
        this.dateOfMed.set(dateOfMed);
    }

    public void setMedDescription(final String medDescription) {
        this.medDescription.set(medDescription);
    }

    public void setMedUnitCost(final BigDecimal medUnitCost) {
        this.medUnitCost.set(medUnitCost);
    }

    public void setMedUnits(final BigDecimal medUnits) {
        this.medUnits.set(medUnits);
    }

    public void setPatientID(final int patientID) {
        this.patientID.set(patientID);
    }

    //Property Objects
    public final IntegerProperty medicalIDProperty() {
        return medicalID;
    }

    public final IntegerProperty medPatientIDProperty() {
        return patientID;
    }

    public final StringProperty medDescriptionProperty() {
        return medDescription;
    }

    public ObjectProperty<LocalDate> dateOfMedProperty() {
        return dateOfMed;
    }

    public ObjectProperty<BigDecimal> medUnitCostProperty() {
        return medUnitCost;
    }

    public ObjectProperty<BigDecimal> medUnitsProperty() {
        return medUnits;
    }

    public StringProperty medDescription() {
        return medDescription;
    }

    //Special TimeStamp
    public final Timestamp getMedTimeStampValue() {
        return Timestamp.valueOf(dateOfMed.get().atStartOfDay());
    }

    public void setMedTimeStampValue(final Timestamp ts) {
        dateOfMed.set(ts.toLocalDateTime().toLocalDate());
    }

    @Override
    public String toString() {
        return "MedicationBean{" + "medicalID=" + medicalID + ", dateOfMed=" + dateOfMed + ", medDescription=" + medDescription + ", medUnitCost=" + medUnitCost + ", medUnits=" + medUnits + ", patientID=" + patientID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.medicalID);
        hash = 37 * hash + Objects.hashCode(this.dateOfMed);
        hash = 37 * hash + Objects.hashCode(this.medDescription);
        hash = 37 * hash + Objects.hashCode(this.medUnitCost);
        hash = 37 * hash + Objects.hashCode(this.medUnits);
        hash = 37 * hash + Objects.hashCode(this.patientID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MedicationBean other = (MedicationBean) obj;
        if (!Objects.equals(this.medicalID, other.medicalID)) {
            return false;
        }
        if (!Objects.equals(this.dateOfMed, other.dateOfMed)) {
            return false;
        }
        if (!Objects.equals(this.medDescription, other.medDescription)) {
            return false;
        }
        if (!Objects.equals(this.medUnitCost, other.medUnitCost)) {
            return false;
        }
        if (!Objects.equals(this.medUnits, other.medUnits)) {
            return false;
        }
        if (!Objects.equals(this.patientID, other.patientID)) {
            return false;
        }
        return true;
    }

}
