/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.beansFX;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 *
 * @author slesperance
 */
public class PatientBean {

    private IntegerProperty patientID;
    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty diagnosis;
    private ObjectProperty<LocalDate> admissionDate;
    private ObjectProperty<LocalDate> releaseDate;
    
    
    public PatientBean(int patientID, final String firstName, final String lastName, final String diagnosis, final Timestamp admissionsDate, final Timestamp releaseDate) {
        this.patientID = new SimpleIntegerProperty(patientID);
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.admissionDate = new SimpleObjectProperty<>(LocalDate.now());
        this.releaseDate = new SimpleObjectProperty<>(LocalDate.now());
        

    }

    public PatientBean() {
        //This still needs to be filled
        //Verify the TimeStamp stuff will actually work.
        this(0, "", " ", " ", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
    }

    public final int getPatientID() {
        return patientID.get();
    }

    public final String getFirstName() {
        return firstName.get();
    }

    public final String getLastName() {
        return lastName.get();
    }

    public final String getDiagnosis() {
        return diagnosis.get();
    }

    public void setPatientID(final int patientID) {
        this.patientID.set(patientID);
    }

    public void setFirstName(final String firstName) {
        this.firstName.set(firstName);
    }

    public void setLastName(final String lastName) {
        this.lastName.set(lastName);
    }

    public void setDiagnosis(final String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    public final IntegerProperty patientIDProperty() {
        return patientID;
    }

    public final StringProperty firstNameProperty() {
        return firstName;
    }

    public final StringProperty lastNameProperty() {
        return lastName;
    }

    public final StringProperty diagnosisProperty() {
        return diagnosis;
    }

    //TimeStamp Issues Location
    
    public final Timestamp getAdmissionsTimeStampValue() {
        return Timestamp.valueOf(admissionDate.get().atStartOfDay());
    }
    
    public final Timestamp getReleaseTimeStampValue() {
        return Timestamp.valueOf(releaseDate.get().atStartOfDay());
    }
    
    public void setAdmissionsTimeStampValue(final Timestamp ts) {
        admissionDate.set(ts.toLocalDateTime().toLocalDate());
    }
    
    public void setReleaseTimeStampValue(final Timestamp ts) {
        releaseDate.set(ts.toLocalDateTime().toLocalDate());
    }
    
    public final ObjectProperty<LocalDate> admissionsDateProperty() {
        return admissionDate;
    }

    public final ObjectProperty<LocalDate> releaseDateProperty() {
        return releaseDate;
    }
    
    public final LocalDate getAdmissionDate() {
        return admissionDate.get();
    }
    
    public final LocalDate getReleaseDate() {
        return releaseDate.get();
    }
    


}
