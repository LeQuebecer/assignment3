/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.beansFX;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author slesperance
 */
public class SurgicalBean {

    //Do I need a Patient ID? No, because the Patient ID will exist and be fetched from the Patient record.
    //I need to implement various rejections as well - can only be X LENGTH etc. no
    private IntegerProperty surgicalID;
    private ObjectProperty<LocalDate> surgeryDate;
    private StringProperty surgeryDescription;
    private ObjectProperty<BigDecimal> roomFee;
    private ObjectProperty<BigDecimal> surgeonFee;
    private ObjectProperty<BigDecimal> supplies;
    private IntegerProperty patientID;

    public SurgicalBean(int patientID, int surgicalID, Timestamp surgeryDate, String surgeryDescription, BigDecimal roomFee, BigDecimal surgeonFee, BigDecimal supplies) {
        this.patientID = new SimpleIntegerProperty(surgicalID);
        this.surgicalID = new SimpleIntegerProperty(patientID);
        this.surgeryDate = new SimpleObjectProperty(LocalDate.now());
        this.surgeryDescription = new SimpleStringProperty(surgeryDescription);
        this.roomFee = new SimpleObjectProperty<>(roomFee);
        this.surgeonFee = new SimpleObjectProperty<>(surgeonFee);
        this.supplies = new SimpleObjectProperty<>(supplies);

    }

    // Require default constructor
    public SurgicalBean() {
        this(0, 0, Timestamp.valueOf(LocalDateTime.now()), " ", BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00));

    }

    //Getters
    public final int getSurgicalID() {
        return surgicalID.get();
    }

    public final String getSurgeryDescription() {
        return surgeryDescription.get();
    }

    public final BigDecimal getRoomFee() {
        return roomFee.get();
    }

    public final BigDecimal getSurgeonFee() {
        return surgeonFee.get();
    }

    public final BigDecimal getSupplies() {
        return supplies.get();
    }

    public final int getPatientID() {
        return patientID.get();
    }
    //Setters

    public void setSurgicalID(final int surgicalID) {
        this.surgicalID.set(surgicalID);
    }

    public void setSurgeryDescription(final String surgeryDescription) {
        this.surgeryDescription.set(surgeryDescription);
    }

    public void setRoomFee(final BigDecimal roomFee) {
        this.roomFee.set(roomFee);
    }

    public void setSurgeonFee(final BigDecimal surgeonFee) {
        this.surgeonFee.set(surgeonFee);
    }

    public void setSupplies(final BigDecimal supplies) {
        this.supplies.set(supplies);
    }

    public void setPatientID(final int patientID) {
        this.patientID.set(patientID);
    }

    //Property Objects
    public final IntegerProperty surgicalIDProperty() {
        return surgicalID;
    }

    public final IntegerProperty patientIDProperty() {
        return patientID;
    }

    public final StringProperty surgeryDescriptionProperty() {
        return surgeryDescription;
    }

    public final ObjectProperty<BigDecimal> suppliesProperty() {
        return supplies;
    }

    public final ObjectProperty<BigDecimal> surgeonFeeProperty() {
        return surgeonFee;
    }

    public final ObjectProperty<BigDecimal> roomFeeProperty() {
        return roomFee;
    }

    //Timestamp Stuff
    public final Timestamp getSurgeryDateTimeStampValue() {
        return Timestamp.valueOf(surgeryDate.get().atStartOfDay());
    }

    public void setSurgeryDateTimeStampValue(final Timestamp ts) {
        surgeryDate.set(ts.toLocalDateTime().toLocalDate());
    }

    public final ObjectProperty<LocalDate> surgeryDateProperty() {
        return surgeryDate;
    }

    public final LocalDate getSurgeryDate() {
        return surgeryDate.get();
    }

    //Other
    @Override
    public String toString() {
        return "SurgicalBean{" + "surgicalID=" + surgicalID + ", surgeryDate=" + surgeryDate + ", surgeryDescription=" + surgeryDescription + ", roomFee=" + roomFee + ", surgeonFee=" + surgeonFee + ", supplies=" + supplies + ", patientID=" + patientID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.surgicalID);
        hash = 97 * hash + Objects.hashCode(this.surgeryDate);
        hash = 97 * hash + Objects.hashCode(this.surgeryDescription);
        hash = 97 * hash + Objects.hashCode(this.roomFee);
        hash = 97 * hash + Objects.hashCode(this.surgeonFee);
        hash = 97 * hash + Objects.hashCode(this.supplies);
        hash = 97 * hash + Objects.hashCode(this.patientID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SurgicalBean other = (SurgicalBean) obj;
        if (!Objects.equals(this.surgicalID, other.surgicalID)) {
            return false;
        }
        if (!Objects.equals(this.surgeryDate, other.surgeryDate)) {
            return false;
        }
        if (!Objects.equals(this.surgeryDescription, other.surgeryDescription)) {
            return false;
        }
        if (!Objects.equals(this.roomFee, other.roomFee)) {
            return false;
        }
        if (!Objects.equals(this.surgeonFee, other.surgeonFee)) {
            return false;
        }
        if (!Objects.equals(this.supplies, other.supplies)) {
            return false;
        }
        if (!Objects.equals(this.patientID, other.patientID)) {
            return false;
        }
        return true;
    }

}
