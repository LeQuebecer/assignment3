/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.assignment3.beansFX;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author slesperance
 */
public class InpatientBean {

    private IntegerProperty inpatientID;
    private ObjectProperty<LocalDate> timeOfStay;
    private StringProperty roomNumber;
    private ObjectProperty<BigDecimal> dailyRate;
    private ObjectProperty<BigDecimal> services;
    private ObjectProperty<BigDecimal> supplies;
    private IntegerProperty patientID;

    public InpatientBean(int patientID, int inpatientID, Timestamp timeOfStay, String roomNumber, BigDecimal dailyRate, BigDecimal services, BigDecimal supplies) {
        this.inpatientID = new SimpleIntegerProperty(inpatientID);
        this.timeOfStay = new SimpleObjectProperty<>(LocalDate.now());
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.dailyRate = new SimpleObjectProperty<>(dailyRate);
        this.services = new SimpleObjectProperty<>(services);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.supplies = new SimpleObjectProperty<>(supplies);
    }

    // Require default constructor
    public InpatientBean() {
        this(0, 0, Timestamp.valueOf(LocalDateTime.now()), " ", BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00));

    }

    // Setters
    public void setInpatientID(final int inpatientID) {
        this.inpatientID.set(inpatientID);
    }

    public void setTimeofStay(Timestamp ts) {
        timeOfStay.set(ts.toLocalDateTime().toLocalDate());
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber.set(roomNumber);
    }

    public void setDailyRate(BigDecimal dailyRate) {
        this.dailyRate.set(dailyRate);
    }

    public void setServices(BigDecimal services) {
        this.services.set(services);
    }

    public void setSupplies(BigDecimal supplies) {
        this.supplies.set(supplies);
    }

    public void setPatientID(final int patientID) {
        //Dont think this should be here, leaving it for now anyways
        this.patientID.set(patientID);
    }

    //Getters
    public int getInpatientID() {
        return inpatientID.get();
    }

    public LocalDate getTimeOfStay() {
        return timeOfStay.get();
    }

    public String getRoomNumber() {
        return roomNumber.get();
    }

    public BigDecimal getDailyRate() {
        return dailyRate.get();
    }

    public BigDecimal getServices() {
        return services.get();
    }

    public BigDecimal getSupplies() {
        return supplies.get();
    }

    public int getPatientID() {
        return patientID.get();
    }
    //Properties

    public final IntegerProperty inpatientIDProperty() {
        return inpatientID;
    }

    public final IntegerProperty patientIDProperty() {
        return patientID;
    }
    
    public final StringProperty roomNumberProperty() {
        return roomNumber;
    }

    public final ObjectProperty<LocalDate> dateTimeOfStayProperty() {
        return timeOfStay;
    }

    public final ObjectProperty<BigDecimal> suppliesProperty() {
        return supplies;
    }

    public final ObjectProperty<BigDecimal> dailyRateProperty() {
        return dailyRate;
    }

    public final ObjectProperty<BigDecimal> servicesProperty() {
        return services;
    }

    //Special TimeStamp Items
    public final Timestamp getTimeOfStayTimeStampValue() {
        return Timestamp.valueOf(timeOfStay.get().atStartOfDay());
    }

    public void setTimeOfStayTimeStampValue(final Timestamp ts) {
        timeOfStay.set(ts.toLocalDateTime().toLocalDate());
    }

    @Override
    public String toString() {
        return "InpatientBean{" + "inpatientID=" + inpatientID + ", timeOfStay=" + timeOfStay + ", roomNumber=" + roomNumber + ", dailyRate=" + dailyRate + ", services=" + services + ", supplies=" + supplies + ", patientID=" + patientID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.inpatientID);
        hash = 29 * hash + Objects.hashCode(this.timeOfStay);
        hash = 29 * hash + Objects.hashCode(this.roomNumber);
        hash = 29 * hash + Objects.hashCode(this.dailyRate);
        hash = 29 * hash + Objects.hashCode(this.services);
        hash = 29 * hash + Objects.hashCode(this.supplies);
        hash = 29 * hash + Objects.hashCode(this.patientID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InpatientBean other = (InpatientBean) obj;
        if (!Objects.equals(this.inpatientID, other.inpatientID)) {
            return false;
        }
        if (!Objects.equals(this.timeOfStay, other.timeOfStay)) {
            return false;
        }
        if (!Objects.equals(this.roomNumber, other.roomNumber)) {
            return false;
        }
        if (!Objects.equals(this.dailyRate, other.dailyRate)) {
            return false;
        }
        if (!Objects.equals(this.services, other.services)) {
            return false;
        }
        if (!Objects.equals(this.supplies, other.supplies)) {
            return false;
        }
        if (!Objects.equals(this.patientID, other.patientID)) {
            return false;
        }
        return true;
    }

}
